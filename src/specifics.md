---
layout: default
title: Implementation details
---
# Implementation details

## Twig specification implementation

Twing aims at implementing Twig specification perfectly, without compromise. This is not an easy task due to the nature of Twig specification: they don't exist officially and can only be deduced from the public documentation, the source code documentation and the test suite of the PHP reference implementation. It sometimes happens that something that was not part of either the documentations or the test suite suddenly becomes part of the specification like the [`filter` tag](https://github.com/twigphp/Twig/issues/3091) or the [macros rework](https://github.com/twigphp/Twig/issues/3090) issues, putting Twing and all other non-reference implementations in the uncomfortable position of having to deal with a potential breaking change. Since Twig's team doesn't plan on releasing some official specification for the language, we can't expect the problem to be solved anytime soon.

Twing's strategy here is to stick strictly to Semantic Versioning rules and *never* introduce a breaking change into a minor version - its extensive test suite with 100% code coverage guarantees that. Twig teams's mistakes will be managed by either issuing a [known issue](#known-issues), if the mistake is trivial, or bumping to a new major version, if it is not.

### Compatibility chart

Here is the compatibility chart between minor versions of Twing and Twig specification levels, along with a summary of notable features provided by each Twig specification level. Note that Twig minor versions don't always provide new language-related features (because of Twig's team perpetuating the confusion between Twig and their reference implementation, TwigPHP).

| Twing version | Twig specification level | Notable features                                                                                                                |
|:-------------:|:------------------------:|---------------------------------------------------------------------------------------------------------------------------------|
|      6.0      |           3.8            | `has every` and `has some` operators, spread operator syntax, removal of the `filter` and `spaceless` tags                      |
|      5.2      |           2.14           | `spaceship` operator, `sort` filter comparator, hash "short" syntax                                                             |
|      3.0      |           2.11           | [Macros scoping](https://twig.symfony.com/doc/2.x/tags/macro.html#macros-scoping)                                               |
|      2.3      |           2.10           | `spaceless`, `column`, `filter`, `map` and `reduce` filters, `apply` tag, `line whitespace trimming` whitespace control modifier |
|      2.2      |           2.6            | `deprecated` tag                                                                                                                |
|      1.3      |           2.5            | `spaceless` and `block`-related deprecations                                                                                    |
|      1.0      |           2.4            |                                                                                                                                 |

It is highly recommended to always use the latest version of Twing available as bug fixes will always target the latest version.

Note that starting with Twing 6.0.0, both Twig specification level 2 and 3 are supported.

## Interpretation of the specification

Twig specification being intimately bound to the TwigPHP implementation, some parts of it don't make sense outside of the context of a PHP runtime. Below is the list of those parts that Twing interprets differently from its TwigPHP counterpart.

## Functions

### `constant`

For Twing, the [constant](https://twig.symfony.com/doc/3.x/functions/constant.html){:target="_blank"} function is analogous to a property accessor.

{% raw %}
```twig
{{ constant('DATE_W3C') }} is analogous to {{ DATE_W3C }}
{{ constant('class', date) }} is analogous to {{ date.class }}
```
{% endraw %}