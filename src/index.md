---
layout: default
title: Home
nav_order: 1
---
Welcome to Twing Documentation
==============================

This is the documentation for Twing, the TypeScript and JavaScript [Twig][language-reference-url] compiler.

## Forewords

In this documentation, the `Twig` term represents the Twig language *specifications*, not the SensioLabs PHP *implementation* of the language - implementation being known as `TwigPHP`. SensioLabs use `Twig` to talk about both the specifications of the language and their own implementation, and their documentation mixes both specifications and PHP implementation details. This should be considered as an oversight instead of an official convention.

Please refer to the [Implementation details]({{ site.baseurl }}{% link specifics.md %}) page for more information on that matter.

## Prerequisites

* Node.js: Twing needs at least **node.js 16.0.0** to run.
* Browser: Twing should run without any issue on every modern browser.

## Installation

The recommended way to install Twing is via npm:

```shell
npm i twing --save
```

## Usage

This section gives you a brief introduction to the TypeScript and JavaScript API of Twing.

### Node.js

{% raw %}
```typescript
import {createEnvironment, createArrayLoader} from "twing";

const loader = createArrayLoader({
    'index.twig': 'Everybody loves {{ name }}!'
});
const environment = createEnvironment(loader);

environment.render('index.twig', {name: 'Twing'}).then((output) => {
    // output contains "Everybody loves Twing!"
});
```
{% endraw %}

Twing uses a loader (`TwingLoaderArray`) to locate templates, and an environment (`TwingEnvironment`) to store the configuration.

The `render()` method loads the template passed as first argument and renders it with the _context_ passed as second argument.

As templates are generally stored on the filesystem, Twing also comes with a filesystem loader:

```typescript
import {createEnvironment, createFilesystemLoader} from "twing";
import * as fs from "fs";

const loader = createFilesystemLoader(fs);
const environment = createEnvironment(loader);

environment.render('index.twig', {name: 'Twing'}).then((output) => {
      // ...
});
```

> Note the filesystem implementation (`fs`) passed as argument to `createFilesystemLoader`. In environments where `fs` is not available, it is perfectly possible to pass another filesystem implementation, as long as it implements the expected interface. Please refer to the API documentation of [createFilesystemLoader]({{ site.twingApiUrl }}/functions/createFilesystemLoader.html) for more information. 

## Browser

Use [jsdelivr](https://www.jsdelivr.com/) CDN to include Twing in your HTML document:

`<script src="https://cdn.jsdelivr.net/npm/twing/dist/lib.min.js"></script>`

Once loaded by the browser, Twing is available under the global `Twing` variable.

{% raw %}
```typescript
const {createEnvironment, createArrayLoader} = Twing;

const loader = createArrayLoader({
    'index.twig': 'Everybody loves {{ name }}!'
});
const environment = createEnvironment(loader);

environment.render('index.twig', {name: 'Twing'}).then((output) => {
    // output contains "Everybody loves Twing!"
});
```
{% endraw %}

### Real-world example using Express

_Credit for this example goes to [stela5](https://github.com/stela5)._

   1. Create working directory and initialize dependencies:

        ```
        mkdir myapp myapp/templates && cd myapp
        npm init -y
        npm install --save express twing
        ```

   2. Create server.js:

        ```javascript
        const app = require('express')();
        const port = process.env.NODE_PORT || 3000;
        const {TwingEnvironment, TwingLoaderFilesystem} = require('twing');
        
        let loader = createFilesystemLoader('./templates', fs);
        let environment = createEnvironment(loader);
        
        app.get('/', function (req, res) {
          environment.render('index.twig', {'name': 'World'}).then((output) => {
              res.end(output);
          });
        });
        
        app.get('/name/:name', function (req, res) {      
          environment.render('index.twig', req.params).then((output) => {
            res.end(output);
          });
        });
        
        app.listen(port, () => {
          console.log('Node.js Express server listening on port '+port);
        });
        ```
   
   4. Start server:

        ```
        node server.js
        ```

[language-reference-url]: {{ site.twigReferenceUrl }}
