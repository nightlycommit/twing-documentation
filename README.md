# Twing Documentation

## Prerequisites

* Ruby Bundler `apt install ruby-bundler`
* Ruby Dev `apt install ruby-dev`

```shell
bundle install
```
## Build

```shell
bundle exec jekyll build -s src -d dist
```

## Serve

```shell
bundle exec jekyll serve -s src -d dist
```